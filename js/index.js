var currentP = location.pathname.toString()
var path = currentP.substring(0, currentP.lastIndexOf("/"))

var user = JSON.parse(sessionStorage.getItem('user'))
if (!user) {
    location.href = path + "/accueil.html"
}

var index = localStorage.getItem('index')
if (!index)
    index = 0

var initName = "task"

$(document).ready(function () {
    $('#userName').text("Welcome " + user.name)
    $('#logOut').click((e) => {
        sessionStorage.removeItem("user")
        location.href = path + "/accueil.html"
    })
    $('#add').click(function (e) {
        let detail = $('#detail').val()
        let count = $('#count').val()
        let name = $('#name').val()
        let infos = {
            'name': name, 'count': count, 'detail': detail
        }
        if (name != '' && detail != '') {
            saveInLocalStorage(infos)
            $('#storageAlert').modal('show')
            $('#modalInfo').text('Succeful save of ' + name + ' for ' + count + ' minnutes')
            $('#detail, #name').val('')
        } else {
            let err = validate(name, detail)
            $('#storageAlert').modal()
            $('#modalHead').text(err.head)
            $('#modalInfo').text(err.info)
            $('.modalClose').addClass('btn-danger')
            $('.modalClose').click((e) => {
                $('#modalHead').text('Task Save')
                var str = '' // after
                $('#modalInfo').text('Task save')
                $('.modalClose').removeClass('btn-danger')
            })
        }
    });
});

function validate(name, detail) {
    var str
    var i = 1
    var head
    if (!name && !detail) {
        str = 'Task name and task detail are required'
        i = 2
    } else if (!name) {
        str = 'Task name is required'
    } else {
        str = 'Task detail is required'
    }
    if (i == 1) {
        head = "one error detected"
    } else {
        head = "two errors detected"
    }
    return { 'head': head, 'info': str }
}

function saveInLocalStorage(infos) {
    try {
        localStorage.setItem(initName + index++, JSON.stringify(infos))
        localStorage.setItem('index', index)
    } catch (e) {
        localStorage.clear()
        saveInLocalStorage()
    }
}