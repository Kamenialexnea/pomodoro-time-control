var currentP = location.pathname.toString()
var path = currentP.substring(0, currentP.lastIndexOf("/"))

var initName = "user"

var user = sessionStorage.getItem(initName)
if (user) {
    location.href = path + "/index.html"
}

$(document).ready (() => {
    $('#login').click((e)=> {
        var name = $('#name').val()
        var password = $('#password').val()
        if (name != '' && password != '') {
            sessionStorage.setItem(initName, JSON.stringify({"name": name, "password": password}))
            $('#personStore').modal('show')
            $('#modalInfo').text(name + ", you have successful log")
            $('.modalClose').click((e) => {
                sessionStorage.removeItem(initName)
            })
        } else {
            let err = validate(name, password)
            $('#personStore').modal('show')
            $('#modalHead').text(err.head)
            $('#modalInfo').text(err.info)
            $('#continue').hide()
            $('.modalClose').click((e) => {
                $('#modalHead').text('Log Status')
                $('#modalInfo').text('')
                $('#continue').show()
            })
        }
        
    })
})

function validate(name, password) {
    var str
    var i = 1
    var head
    if (!name && !password) {
        str = 'Your name and your password are required'
        i = 2
    } else if (!name) {
        str = 'Your name is required'
    } else {
        str = 'Your password is required'
    }
    if (i == 1) {
        head = "one error detected"
    } else {
        head = "two errors detected"
    }
    return {'head':head, 'info': str}
}