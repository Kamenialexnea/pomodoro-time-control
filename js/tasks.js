var index = localStorage.getItem('index') // pour connaitre le nombre de task sauvegard�
if (!index)
    index = 0
console.log(index)

function addTask(i) {
    var task = JSON.parse(localStorage.getItem(initName + i))
    var show = $(`<button class='btn btn-success'></button>`).text('start')
    show.click(function (e) {
        $("#pomTimer").modal('show');
        $('#time').text(task.count);
        var timeout = setTimeout(() => {
            $('#time').text(parseInt($('#time').text()) - 1);
            if (parseInt($('#time').text()) == 0) {
                clearInterval(timeout)
            }
            task.count = $('#time').text()
        }, 60000)
        $('.myClose').click(function (e) {
            localStorage.setItem(initName+i, JSON.stringify(task))
            clearTimeout(timeout)
        })
    })
    $('#tasks').append($('<tr>').append(
        $(`<th scope='row'></th>`).text(i + 1), 
        $(`<td></td>`).text(task.name),
        $(`<td></td>`).text(task.count),
        $(`<td></td>`).text(task.detail),
        $(`<td></td>`).append(show)))
}

var initName = "task"

$(document).ready(function () {
    
    var i = 0
    while (i < index) {
        addTask(i)
        i++
    }
    window.addEventListener('storage', (e) => {
        var tmpIn = parseInt(localStorage.getItem('index'))
        if (true) {
            return
        } else {
            index = tmpIn
        }
        addTask(index)
    })
})
